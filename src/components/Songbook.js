import axios from 'axios';
import {useEffect, useState} from "react";
import {Button, TextField} from "@material-ui/core";
import {combineLatestWith, interval, map, Observable, take} from "rxjs";

const Songbook = () => {
    const songbookUrl = 'http://localhost:3004/songbook/';
    const [songbookList, setSongbookList] = useState([]);
    const [updateSongbook, setUpdateSongbook] = useState(false);
    const [displayedListener, setdisplayedListener] = useState({});
    const [favoriteAlbum, setFavoriteAlbum] = useState({});
    const [isEditable, setIsEditable] = useState(true);

    // interface Listeners {
    //     name: string;
    //     username: string;
    //     company: {
    //         name: string;
    //     }
    // }

    useEffect(() => {
        fetch(songbookUrl)
            .then(response => response.json())
            .then(myJson => {
                setSongbookList(myJson)
            })
    }, [updateSongbook])

    useEffect(() => {
        const listeners$ = returnUsersSub();
        const albums$ = returnAlbumsSub();

        if (!listeners$ || !albums$) {
            return;
        }

        listeners$.pipe(combineLatestWith(albums$)).subscribe(([listener, album]) => {
            console.log(album);
        })

        return () => listeners$.unsubscribe();
    }, [])

    const returnUsersSub = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(myJson => {
                return interval(3000).pipe(take(myJson.length), map(index => myJson[index]))
                    .subscribe(listener => setdisplayedListener(listener));
            })
    }

    const returnAlbumsSub = () => {
        fetch('https://jsonplaceholder.typicode.com/albums')
            .then(response => response.json())
            .then(myJson => {
                return interval(5000).pipe(take(myJson.length), map(index => myJson[index])).subscribe(album => console.log(album));
            })
    }

    const handleFieldUpdate = (ev, field) => {
        const elementId = ev.target.closest('.song-row').id;

        updateSongField(elementId, field, ev.target.value);
    }

    const updateSongField = (id, field, value) => {
        const observable$ = new Observable(observer => {
            axios.patch(songbookUrl + id, {[field]: value}).then(() => {
                observer.next(!updateSongbook);
            })
        })

        const subscription = observable$.subscribe((data) => {
            setUpdateSongbook(data);
        })

        return () => subscription.unsubscribe();
    }

    return (
        <>
            <h5>Songs List</h5>
            <div className="songs-list">
                {songbookList.map(song => {
                    return <div className="song-row" id={song.id}>
                        <div className="song-details">
                            <TextField
                                label={"Band:"}
                                value={song.band}
                                disabled={isEditable}
                                onChange={(ev) => handleFieldUpdate(ev, 'band')}
                            />
                            <TextField
                                label={"Title:"}
                                value={song.title}
                                disabled={isEditable}
                                onChange={(ev) => handleFieldUpdate(ev, 'title')} />
                            <TextField
                                label={"BPM:"}
                                value={song.bpm}
                                disabled={isEditable}
                                onChange={(ev) => handleFieldUpdate(ev, 'bpm')}
                            />

                            <TextField
                                label={"Key:"}
                                value={song.key}
                                disabled={isEditable}
                                onChange={(ev) => handleFieldUpdate(ev, 'key')}
                            />
                        </div>
                        <Button onClick={() => setIsEditable(!isEditable)}>{isEditable ? "Edit" : "Disable"}</Button>
                    </div>
                })}
            </div>
            <div>
                <button>Add new song</button>
            </div>

            {displayedListener.name &&
            <div id="listener-container"><span>{displayedListener.username}</span> likes the band <span>{displayedListener.company?.name}</span></div>
            }
        </>
    )
}

export default Songbook;
