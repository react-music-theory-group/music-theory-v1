import {TheoryInfo} from "../Data";
import {from, pairwise} from "rxjs";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

const ScalePattern = (props) => {
    const {keyQuality} = props;
    const [patternSteps, setPatternSteps] = useState([]);


    useEffect(() => {
        const pattern = from(TheoryInfo[keyQuality + 'IntervalPattern']).pipe(pairwise());
        const patternSubscription = pattern.subscribe((val) => {
            setPatternSteps(patternSteps => patternSteps.concat(val[1] - val[0] === 1 ? 'h' : 'W'));
        });
        return () => patternSubscription.unsubscribe();
    }, [])

    return (
        <>
            <div>{TheoryInfo[keyQuality + 'IntervalPattern']}</div>
            <div>{patternSteps}</div>
            <Link to="/">Home</Link>
        </>
    )
}

export default ScalePattern;
