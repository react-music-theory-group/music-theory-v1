import {Slider} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import {interval, merge, map, Observable, Subscription} from "rxjs";

const Metronome = () => {
    const [bpm, setBpm] = useState<number>(60);
    const [metronomeHighlight, setMetronomeHighlight] = useState<boolean>(false);

    const handleBpmChange = (event, newValue) => {
        setBpm(newValue);
    }

    useEffect(() => {
        const metronome: Observable<any> =  interval(60 / bpm * 1000);
        // const metronomeRest =  interval(60 / bpm * 1000 + (60 / bpm * 1000) / 2);
        const metronomeRest: Observable<any> =  interval((60 / bpm * 1000) + 200);

        const metronomeSubscription: Subscription = merge(metronome.pipe(map(() => 'showing')),
            metronomeRest.pipe(map(() => 'hiding'))).subscribe(val => {
            console.log(val);
            setMetronomeHighlight(!metronomeHighlight);
        })

        return () => metronomeSubscription.unsubscribe();
    }, [metronomeHighlight, bpm])

    return (
        <>
            <Slider value={bpm} onChange={handleBpmChange} valueLabelDisplay="auto" min={40} max={140}/>
            <div className={"indicator" + (metronomeHighlight ? ' highlight': ' default')}>Metronome indicator</div>
            <Link to="/">Home</Link>
        </>
    )
}

export default Metronome;
