import {pointsColumns} from "./StudyGoals";

const SkillTree = (props) => {
    const skillTreeJSX = [];
    pointsColumns.id.forEach((id, index) => {
        skillTreeJSX.push(
            <div id={id}>
                <label>{pointsColumns.label[index]}</label>
                <SkillColumns skillPoints={eval('props.skillPointsCol' + (index + 1))} />
            </div>)
    })
    return (
        <>
            {skillTreeJSX}
        </>
    )
}

export default SkillTree;

const SkillColumns = (props) => {
    const skillPointsJSX = [];
    let skillPointsIteration = props.skillPoints;
    while (skillPointsIteration) {
        skillPointsJSX.push(<div className='skill-point'></div>);
        skillPointsIteration--;
    }
    return (
        <div className='skill-container'>
            {skillPointsJSX}
        </div>
    )
}
