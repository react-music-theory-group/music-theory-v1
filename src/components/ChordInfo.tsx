import {TheoryInfo} from "../Data.js";
import {Link} from "react-router-dom";

const ChordInfo = (props) => {
    const {chord, keyQuality, scale, notesInChord} = props;
    const chordQuality: string = TheoryInfo.chordsQuality[keyQuality][scale.indexOf(chord)];

    return (
        <>
            <div className="chord-notes-container">
                <h3>These are the notes in {chord + ' ' + chordQuality} chord</h3>
                <div className="chord-notes">
                    {notesInChord.map((note, i) => <span key={i}>{note}</span>)}
                </div>
            </div>
            <Link to="/">Home</Link>
        </>
    )
}

export default ChordInfo;
