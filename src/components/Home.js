import {FormControl, FormControlLabel, FormLabel, InputLabel, MenuItem, Radio, RadioGroup, Select, TextField, Tooltip} from "@material-ui/core";
import {TheoryInfo} from "../Data";
import {Link} from "react-router-dom";
import StudyGoals from "./StudyGoals";

const Home = (props) => {
    const {musicKey, keyQuality, scale, chord, handleInputChange, handleSelectChord} = props;

    return (
        <>
            <header>
                <h3>Showing info about <span className="highlight">{musicKey + ' ' + keyQuality}</span> key</h3>
            </header>

            <TextField
                className="select-key"
                label="Key"
                variant="filled"
                value={musicKey}
                onChange={(e) => {
                    handleInputChange('key', e.target.value.toUpperCase())
                }}
            />

            <FormControl className="key-quality">
                <InputLabel id="key-quality-label">Key Quality</InputLabel>
                <Select
                    labelId="key-quality-label"
                    id="key-quality-select"
                    value={keyQuality}
                    onChange={(e) => handleInputChange('keyQuality', e.target.value)}
                >
                    <MenuItem value="major">Major</MenuItem>
                    <MenuItem value="minor">Minor</MenuItem>
                </Select>
            </FormControl>

            <div>
                <Link to="/scale-pattern">Scale pattern</Link>
                <Tooltip title={TheoryInfo[keyQuality + 'IntervalPattern']}>
                    <div>Scale Pattern</div>
                </Tooltip>
            </div>

            {musicKey && keyQuality &&
            <div className="note-chord-container">
                <div className="key-notes">
                    <div>Notes in key</div>
                    {scale.map((note, i) => <div key={i}>{note}</div>)}
                </div>

                <div className="key-chords">
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Chords in key</FormLabel>
                        <RadioGroup value={chord} onChange={(e) => handleSelectChord(e.target.value)}>
                            {scale.map((chordInst, i) =>
                                <div className="chord-radio" key={i}>
                                    <FormControlLabel
                                        value={chordInst}
                                        label={chordInst + ' ' + TheoryInfo.chordsQuality[keyQuality][i]}
                                        control={<Radio />}
                                    />
                                    {chord === chordInst &&
                                    <Link className="chord-notes-link" to="/notes-in-chord">Chord Info</Link>
                                    }
                                </div>
                            )}
                        </RadioGroup>
                    </FormControl>
                </div>
            </div>
            }

            <StudyGoals/>

            <div>
                <Link to="/songbook">Songbook</Link>
            </div>
        </>
    )
}

export default Home;
