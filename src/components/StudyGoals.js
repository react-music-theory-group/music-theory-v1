import {fromEvent, combineLatestWith, mapTo, take, takeWhile, share, publishReplay, refCount, Subject, multicast, connect, BehaviorSubject} from "rxjs";
import {useEffect, useState} from "react";
import SkillTree from "./SkillTree";

export const pointsColumns = {
    id: ['theory', 'shred', 'phrasing'],
    label: ['Theory', 'Shredding & chops', 'Phrasing in solos'],
};

const StudyGoals = () => {
    const [skillPointsCol1, setSkillPointsCol1] = useState(0);
    const [skillPointsCol2, setSkillPointsCol2] = useState(0);
    const [skillPointsCol3, setSkillPointsCol3] = useState(0);

    const totalPoints = 10;
    const currentPointsCount = skillPointsCol1 + skillPointsCol2 + skillPointsCol3;
    const freePoints = totalPoints - currentPointsCount;

    useEffect(() => {
        const firstButton$ = fromEvent(document.getElementById(pointsColumns.id[0]), 'click')
            .pipe(takeWhile(() => checkSkillPointsAvailable(skillPointsCol1)), mapTo(pointsColumns.id[0]));
        const secondButton$ = fromEvent(document.getElementById(pointsColumns.id[1]), 'click')
            .pipe(takeWhile(() => checkSkillPointsAvailable(skillPointsCol2)), mapTo(pointsColumns.id[1]));
        const thirdButton$ = fromEvent(document.getElementById(pointsColumns.id[2]), 'click')
            .pipe(takeWhile(() => checkSkillPointsAvailable(skillPointsCol3)), mapTo(pointsColumns.id[2]));

        const firstButtonSubscription = firstButton$.subscribe(() => {
            setSkillPointsCol1(skillPointsCol1 + 1);
        })

        const secondButtonSubscription = secondButton$.subscribe(() => {
            setSkillPointsCol2(skillPointsCol2 + 1);
        })

        const thirdButtonSubscription = thirdButton$.subscribe(() => {
            setSkillPointsCol3(skillPointsCol3 + 1);
        })

        // const choicesSubscription = firstButton$.pipe(combineLatestWith(secondButton$, thirdButton$), take(1)).subscribe(([one, two, three]) => {
        //     console.log(one, two, three)
        // })

        return () => {
            firstButtonSubscription.unsubscribe();
            secondButtonSubscription.unsubscribe();
            thirdButtonSubscription.unsubscribe();
            // choicesSubscription.unsubscribe();
        }
    }, [skillPointsCol1, skillPointsCol2, skillPointsCol3])

    const checkSkillPointsAvailable = (skillPointsCol) => {
        if ((skillPointsCol1 + skillPointsCol2 + skillPointsCol3 < 10) && (skillPointsCol < 8)) {
            if (freePoints === 1) {
                if (skillPointsCol1 >= 1 && skillPointsCol2 >= 1 && skillPointsCol3 >= 1) {
                    return true;
                } else {
                    return skillPointsCol === 0;
                }
            }
            return true;
        }
        return false;
    }

    return (
        <div className="goal-buttons">
            <h5>What do you want to improve on?</h5>
            <SkillTree skillPointsCol1={skillPointsCol1} skillPointsCol2={skillPointsCol2} skillPointsCol3={skillPointsCol3} />
        </div>
    )
}

export default StudyGoals;
