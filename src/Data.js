export const TheoryInfo = {
    notes: ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'],
    majorIntervalPattern: [0, 2, 4, 5, 7, 9, 11],
    minorIntervalPattern: [0, 2, 3, 5, 7, 8, 10],
    chordsQuality: {
        major: ['major', 'minor', 'minor', 'major', 'major', 'minor', 'diminished'],
        minor: ['minor', 'diminished', 'major', 'minor', 'minor', 'major', 'major']
    },
};
