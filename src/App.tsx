import './App.scss';
import {useEffect, useState} from "react";
import {TheoryInfo} from "./Data.js";
import {Switch, Route, Link, useLocation} from "react-router-dom";
import Home from "./components/Home";
import ChordInfo from "./components/ChordInfo";
import ScalePattern from "./components/ScalePattern";
import Metronome from "./components/Metronome";
import {filter, interval, Observable, Subscription, take} from "rxjs";
import Songbook from "./components/Songbook";

function App() {
	const [musicKey, setMusicKey] = useState<string>('');
	const [keyQuality, setKeyQuality] = useState<string>('major');

	const [chord, setChord] = useState<string>('');
	const [scale, setScale] = useState<string[]>([]);
	const [notesInChord, setNotesInChord] = useState<string[]>([]);
	const [practiceTime, setPracticeTime] = useState<number>(0);
	const [pausePracticeTimer, setPausePracticeTimer] = useState<boolean>(false);
	const [stopPracticeTimer, setStopPracticeTimer] = useState<boolean>(false);

	const notes: string[] = TheoryInfo.notes;
	const majIntPattern: number[] = TheoryInfo.majorIntervalPattern;
	const minIntPattern: number[] = TheoryInfo.minorIntervalPattern;

	const location = useLocation();

	const {v4: uuidv4} = require('uuid');

	useEffect(() => {
		setScaleFunction();
	}, [musicKey, keyQuality])

	useEffect(() => {
		setNotesInChordAlgo();
	}, [chord])

	useEffect(() => {
		let practiceCounter = interval(5000).pipe(filter(val => !pausePracticeTimer));
		const practiceSubscription = practiceCounter.subscribe(val => {
			setPracticeTime(val + 1);
		});

		const pausePracticeCounter: Observable<any> = interval(10000).pipe(filter(val => pausePracticeTimer), take(1));
		const pausePracticeCounterSubscription: Subscription = pausePracticeCounter.subscribe(() => setPausePracticeTimer(false));

		if (pausePracticeTimer) {
			setPracticeTime(0);
		}

		return () => {
			practiceSubscription.unsubscribe();
			pausePracticeCounterSubscription.unsubscribe();
		}

	}, [pausePracticeTimer])

	const rearrangeNotesArray = (list: string[], newRootIndex: number): string[] => {
		return list.slice(newRootIndex).concat(list.slice(0, newRootIndex));
	}

	const setScaleFunction = () => {
		if (!musicKey || !keyQuality) {
			return;
		}
		setChord('');

		let pattern: number[];
		const selectedScale: string[] = [];
		let scaleStartingWithRoot: string[] = [];
		keyQuality === 'major' ? pattern = majIntPattern : pattern = minIntPattern;
		const noteIndex: number = notes.indexOf(musicKey);

		scaleStartingWithRoot = rearrangeNotesArray(notes, noteIndex);
		pattern.forEach((val: number) => {
			selectedScale.push(scaleStartingWithRoot[val]);
		})

		setScale(selectedScale);
	}

	const handleInputChange = (prop: string, val: string) => {
		prop === 'key' ? setMusicKey(val) : setKeyQuality(val);
	}

	const handleSelectChord = (selectedChord: string) => {
		const selectedChordIndex: number = scale.indexOf(selectedChord);
		setChord(scale[selectedChordIndex]);
	}

	const setNotesInChordAlgo = () => {
		const mode: string[] = rearrangeNotesArray(scale, scale.indexOf(chord));
		const root: string = mode[0];
		const third: string = mode[2];
		const fifth: string = mode[4];
		setNotesInChord([root, third, fifth]);
	}

	return (
		<div className="App">
			<header className="">
				<h2>Circle of Fifths Music Theory App</h2>
			</header>

			<div className="main-content">
				<Switch>
					<Route exact path="/">
						<Home musicKey={musicKey} keyQuality={keyQuality} scale={scale}
							  handleInputChange={handleInputChange} handleSelectChord={handleSelectChord} chord={chord}
						/>
					</Route>
					<Route path="/scale-pattern">
						<ScalePattern keyQuality={keyQuality} />
					</Route>
					<Route path="/notes-in-chord">
						<ChordInfo keyQuality={keyQuality} chord={chord} scale={scale} notesInChord={notesInChord} />
					</Route>
					<Route path="/metronome"
						   render={() => <Metronome />}
					/>
					<Route path="/songbook"
						   render={() => <Songbook />}
					/>
				</Switch>

				{practiceTime !== 0 && !pausePracticeTimer && !stopPracticeTimer &&
                <div className="practice-notice">
                    <span>You haven't practiced in {practiceTime} minute{practiceTime === 1 ? '' : 's'}</span>
                    <button onClick={() => setPausePracticeTimer(true)}>Pause</button>
                    <button onClick={() => setStopPracticeTimer(true)}>Stop</button>
                </div>
				}

				{location.pathname !== '/metronome' &&
                <Link to="/metronome">Metronome</Link>
				}
			</div>
		</div>
	)
}

export default App;
